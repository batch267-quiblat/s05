<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S05: Activity - Login</title>
	</head>
	<body>

		<?php session_start(); ?> 

		<form method="POST" action="./server.php">

			<input type="hidden" name="action" value="login">

			<label for="email">Email:</label>
			<input type="email" name="email" required>

			<label for="password">Password:</label>
			<input type="password" name="password" required>

			<button type="submit">Login</button>
		</form>
	</body>
</html>