<?php
	// $_GET and $_POST are "super global" variables in PHP.
		// Allow you to retrieve information sent by the client.

	// "super global" variable allows data to persists between pages or a single session.

	//Both $_GET and $_POST creates an associative array that holds key=>value pair.
	 	// "key" represents the name of the form control element.
		// "value" represents the inputted data from the user.

	// var_dump($_GET);
	// var_dump($_POST);
	
	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	// isset() function checks whether a variable is set.
		// true is a variable/key is set, false if the return is NULL.
		// This is used to test the form is submitted successful or not.
		// This is also used to avoid any warnings in our browser when accessing the "index" key of are $_GET array
	if(isset($_GET["index"])){
		// $_GET is an array of variables passed to the current script via the URL parameters.
		$indexGet = $_GET["index"];
		echo "The retrieved task from GET is $tasks[$indexGet]. <br>";
	}

	if(isset($_POST["index"])){
		// $_POST is an array of variables passed to the current script via the HTTP POST method.
		$indexPost = $_POST["index"];
		echo "The retrieved task from POST is $tasks[$indexPost]. <br>";
	}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05: Client-Server Communication ($_GET and $_POST)</title>
	</head>
	<body>
		<!-- 

			GET VS POST

			GET METHOD
				- Information sent is visible to everyone (all variable names and values are displayed in the URL.)
				- It also has limits (3000 characters) to the amount of information to send.
				- This may be used for sending non-sensitive data.

			POST METHOD
				- Information sent is invisible to others (all names/values are embedded within the body of the HTTP request.)
				- has no limits on the amount of information to send.
				- Developers prefer POST for sending form data.
		 -->
	    
		<h1>Task index from GET</h1>
		<form method="GET">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">GET</button>

			<!-- <label>Email:</label>
			<input type="email" name="email">
			<br>
			<label>Password:</label>
			<input type="password" name="password">
			<br>
			<button type="submit">Login</button> -->
		</form>

		<h1>Task index from POST</h1>
		<form method="POST">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">POST</button>
		</form>
	</body>
</html>