<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05</title>
	</head>


	<body>
		<!-- 
			Session
				-A session is a way to store information (i variable) to be used across multiple pages.
				-Session varibles hold information about one single user and are available to all pages in on application
		 -->

		 <!-- Start the session -->
		 <!-- Reminder: It must be the very first thing on your document before any html tags -->
		 <?php session_start(); ?>

		<!-- This Section will be responsible for adding task on our lists -->
		<h3>Add Task</h3>

		<form method = "POST" action = "./server.php">
			<input type="hidden" name="action" value="add">
			Description: <input type="text" name="description" required>
			<button type="submit">Add</button>

		</form>


		<!-- <pre><?php var_dump($_SESSION['tasks']) ?></pre> -->

		<br>
		<h3>Task Lists</h3>

		<!-- 
			-$id represents the index number of a specific task in the sessions variable
			-$task represents the exact object stored in the sessions variable
		 -->

		<?php if (isset($_SESSION['tasks'])): ?>

			<?php foreach($_SESSION['tasks'] as $id => $task): ?>

				<div>
					<form method = "POST" action = "./server.php" style = "display: inline-block;">
						<input type="hidden" name="action" value = 'update'>
						<input type="hidden" name="id" value = "<?php echo $id; ?>">
						<input type="checkbox" name="isFinished" <?php echo ($task ->isFinished) ? 'checked' : null; ?>>
						<input type="text" name="description" value = '<?php echo $task ->description; ?>'>

						<input type="submit" value="Update">
					</form>

					<!-- form for deleting a task -->
					<form style = "display: inline-block;" method ="POST" action ="./server.php">
						<input type="hidden" name="action" value = "delete">
						<input type="hidden" name="id" value = "<?php echo $id; ?>">
						<input type="submit" value="Delete">
					</form>
				</div>

			<?php endforeach; ?>

		<?php endif; ?>

		<h3>Delete all tasks</h3>
		<form method= "POST" action ='./server.php'>
			<input type="hidden" name="action" value="clear">
			<button type="submit">Clear all tasks</button>
		</form>

		


	</body>
</html>